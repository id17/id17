
from bliss.controllers.motor import CalcController
from bliss.common.logtools import log_debug

class multiplymot(CalcController):
    def initialize(self):
        CalcController.initialize(self)
        self.factor = self.config.get("factor",1)
        log_debug(self, "tth factor is %s" % self.factor)

    def initialize_axis(self, axis):
        CalcController.initialize_axis(self,axis)

    def calc_from_real(self, pos):
        log_debug(self, "calculating from real %s" % str(pos))
        mpos = pos['real_mot']
        return {'calc_mot': mpos * self.factor}

    def calc_to_real(self, pos):
        log_debug(self, "calculating to real %s" % str(pos))
        mpos = pos['calc_mot']
        return {'real': mpos / self.factor }

